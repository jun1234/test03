//
//  main.m
//  lhl
//
//  Created by 刘宏立 on 16/6/17.
//  Copyright © 2016年 lhl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
